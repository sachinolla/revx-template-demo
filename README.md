# RevX Layouts
------------------------------

## 1. multi_slot_banner
---------------------------

### 300x50
![Alt text](templates/multi_slot_banner/300x50.png?raw=true "300x50")
### 320x50
![Alt text](templates/multi_slot_banner/320x50.png?raw=true "320x50")
### 728x90
![Alt text](templates/multi_slot_banner/728x90.png?raw=true "728x90")


## 2. carousel_2_products
--------------------------
### 300x50
![Alt text](templates/carousel_2_products/300x50.png?raw=true "300x50")
### 320x50
![Alt text](templates/carousel_2_products/320x50.png?raw=true "320x50")
### 300x250
![Alt text](templates/carousel_2_products/300x250.png?raw=true "300x250")

## 3. carousel_4_products
-------------------------
### 320x480
![Alt text](templates/carousel_4_products/320x480.png?raw=true "320x480")
### 480x320
![Alt text](templates/carousel_4_products/480x320.png?raw=true "480x320")

## 4. richmedia_swiper
-------------------------
### 320x480
![Alt text](templates/richmedia_swiper/320x480_a.png?raw=true "320x480")
![Alt text](templates/richmedia_swiper/320x480_b.png?raw=true "320x480")

## 5. vanilla_scroller
-------------------------
### 320x480
![Alt text](templates/vanilla_scroller/320x480.png?raw=true "320x480")

## 6. circle_animation
--------------------------
### 200x200
![Alt text](templates/circle_animation/200x200.png?raw=true "200x200")
### 250x250
![Alt text](templates/circle_animation/250x250.png?raw=true "250x250")
### 300x250
![Alt text](templates/circle_animation/300x250.png?raw=true "300x250")
### 336x280
![Alt text](templates/circle_animation/336x280.png?raw=true "336x280")
### 480x320
![Alt text](templates/circle_animation/480x320.png?raw=true "480x320")

## 7. banner_slider
-------------------------
### 300x50
![Alt text](templates/banner_slider/300x50_a.png?raw=true "300x50")
![Alt text](templates/banner_slider/300x50_b.png?raw=true "300x50")
### 320x50
![Alt text](templates/banner_slider/320x50_a.png?raw=true "320x50")
![Alt text](templates/banner_slider/320x50_b.png?raw=true "320x50")

## 8. queue_tap_slider
-------------------------
### 300x250
![Alt text](templates/queue_tap_slider/300x250.png?raw=true "300x250")

## 9. focused_6_product
-------------------------
### 300x250
![Alt text](templates/focused_6_product/300x250.png?raw=true "300x250")
### 320x480
![Alt text](templates/focused_6_product/320x480.png?raw=true "320x480")
### 480x320
![Alt text](templates/focused_6_product/480x320.png?raw=true "480x320")

## 9. focused_8_layout
-------------------------
### 320x480
![Alt text](templates/focused_8_layout/320x480.png?raw=true "320x480")

## 10. price_drop_grid
-------------------------
### 300x250
![Alt text](templates/price_drop_grid/300x250_a.png?raw=true "300x250")
![Alt text](templates/price_drop_grid/300x250_b.png?raw=true "300x250")


## 11. multi_prod_flipper
-------------------------
### 300x250
![Alt text](templates/multi_prod_flipper/300x250.png?raw=true "300x250")


## 12. tab_swiper
-------------------------
### 320x480
![Alt text](templates/tab_swiper/320x480.png?raw=true "320x480")


## 13. vertical_multiple_slider
-------------------------
### 300x250
![Alt text](templates/vertical_multiple_slider/300x250.png?raw=true "300x250")


## 14. single_slot
-------------------------
### 320x480
![Alt text](templates/single_slot/320x480.png?raw=true "320x480")

## 15. multi_product_scroller
-------------------------
### 320x480
![Alt text](templates/multi_product_scroller/320x480_a.png?raw=true "320x480")
![Alt text](templates/multi_product_scroller/320x480_b.png?raw=true "320x480")

## 16. multi_product_scroller_v2
-------------------------
### 320x480
![Alt text](templates/multi_product_scroller_v2/320x480.png?raw=true "320x480")

## 17. new_focused_product
-------------------------
### 250x250
![Alt text](templates/new_focused_product/v1/250x250.png?raw=true "250x250")

### 300x50
![Alt text](templates/new_focused_product/v2/300x50.png?raw=true "300x50")

### 300x250
![Alt text](templates/new_focused_product/v1/300x250.png?raw=true "300x250")

### 320x50
![Alt text](templates/new_focused_product/v2/320x50.png?raw=true "320x50")

### 320x100
![Alt text](templates/new_focused_product/v1/320x100.png?raw=true "320x100")

### 320x480
![Alt text](templates/new_focused_product/v1/320x480_1.png?raw=true "320x480")
![Alt text](templates/new_focused_product/v1/320x480_2.png?raw=true "320x480")
![Alt text](templates/new_focused_product/v1/320x480_2.png?raw=true "320x480")

### 480x320
![Alt text](templates/new_focused_product/v1/480x320.png?raw=true "480x320")

## 18. product_view
-------------------------
### 250x250
![Alt text](templates/product_view/v1/250x250_v1.png?raw=true "250x250")
![Alt text](templates/product_view/v2/250x250_v2.png?raw=true "250x250")

### 300x50
![Alt text](templates/product_view/v1/300x50_v1.png?raw=true "300x50")

### 320x50
![Alt text](templates/product_view/v1/320x50_v1.png?raw=true "320x50")
![Alt text](templates/product_view/v4/320x50_v4.png?raw=true "320x50")

### 300x250
![Alt text](templates/product_view/v2/300x250_v2.png?raw=true "300x250")
![Alt text](templates/product_view/v3/300x250_v3.png?raw=true "300x250")

### 320x100
![Alt text](templates/product_view/v1/320x100_v1.png?raw=true "320x100")
![Alt text](templates/product_view/v4/320x100_v4.png?raw=true "320x100")

### 320x480
![Alt text](templates/product_view/v2/320x480_v2.png?raw=true "320x480")
![Alt text](templates/product_view/v3/320x480_v3.png?raw=true "320x480")

### 480x320
![Alt text](templates/product_view/v3/480x320_v3.png?raw=true "480x320")
![Alt text](templates/product_view/v5/480x320_v5.png?raw=true "480x320")


## 18. descriptive_layer_flicker
-------------------------
### 250x250
![Alt text](templates/descriptive_layer_flicker/v1/250x250_v1.png?raw=true "250x250")

### 300x250
![Alt text](templates/descriptive_layer_flicker/v1/300x250_v1.png?raw=true "300x250")
![Alt text](templates/descriptive_layer_flicker/v2/300x250_v2_1.png?raw=true "300x250")
![Alt text](templates/descriptive_layer_flicker/v2/300x250_v2_2.png?raw=true "300x250")

### 300x50
![Alt text](templates/descriptive_layer_flicker/v3/300x50_v3.png?raw=true "300x50")

### 320x100
![Alt text](templates/descriptive_layer_flicker/v4/320x100_v4.png?raw=true "320x100")

### 320x480
![Alt text](templates/descriptive_layer_flicker/v1/320x480_v1.png?raw=true "320x480")
![Alt text](templates/descriptive_layer_flicker/v2/320x480_v2_1.png?raw=true "320x480")
![Alt text](templates/descriptive_layer_flicker/v2/320x480_v2_2.png?raw=true "320x480")

### 320x50
![Alt text](templates/descriptive_layer_flicker/v3/320x50_v3.png?raw=true "320x50")

### 480x320
![Alt text](templates/descriptive_layer_flicker/v5/480x320_v5.png?raw=true "480x320")


## 18. grid_layout
-------------------------
### 200x200
![Alt text](templates/grid_layout/v1_200x200.png?raw=true "200x200")
![Alt text](templates/grid_layout/v2_200x200.png?raw=true "200x200")

### 250x250
![Alt text](templates/grid_layout/v1_250x250.png?raw=true "250x250")
![Alt text](templates/grid_layout/v2_250x250.png?raw=true "250x250")

### 300x250
![Alt text](templates/grid_layout/v1_300x250.png?raw=true "300x250")
![Alt text](templates/grid_layout/v2_300x250.png?raw=true "300x250")

### 320x480
![Alt text](templates/grid_layout/v1_320x480.png?raw=true "320x480")
![Alt text](templates/grid_layout/v2_320x480.png?raw=true "320x480")

### 336x280
![Alt text](templates/grid_layout/v1_336x280.png?raw=true "336x280")
![Alt text](templates/grid_layout/v2_336x280.png?raw=true "336x280")

### 480x320
![Alt text](templates/grid_layout/v1_480x320.png?raw=true "480x320")
![Alt text](templates/grid_layout/v2_480x320.png?raw=true "480x320")


## 18. thumbnail_slider
-------------------------
### 320x480
![Alt text](templates/thumbnail_slider/320x480.png?raw=true "320x480")

## 19. layer_flicker
-------------------------
### 320x480
![Alt text](templates/layer_flicker/320x480_v1.png?raw=true "320x480")
![Alt text](templates/layer_flicker/320x480_v2.png?raw=true "320x480")

## 20. peek_animation
-------------------------
### 480x320
![Alt text](templates/peek_animation/480x320_1.png?raw=true "480x320")
![Alt text](templates/peek_animation/480x320_2.png?raw=true "480x320")

## 21. curved_product_swiper
-------------------------
### 320x480
![Alt text](templates/curved_product_swiper/320x480_1_1.png?raw=true "320x480")
![Alt text](templates/curved_product_swiper/320x480_1_2.png?raw=true "320x480")
![Alt text](templates/curved_product_swiper/320x480_2_1.png?raw=true "320x480")
![Alt text](templates/curved_product_swiper/320x480_2_2.png?raw=true "320x480")


### 480x320
![Alt text](templates/curved_product_swiper/480x320_1.png?raw=true "480x320")
![Alt text](templates/curved_product_swiper/480x320_2.png?raw=true "480x320")


## 22. plane_postmate
-------------------------
### 320x480
![Alt text](templates/plane_postmate/320x480.png?raw=true "320x480")


## 23. cube_meta
-------------------------
### 320x480
![Alt text](templates/cube_meta/320x480_1.png?raw=true "320x480")
![Alt text](templates/cube_meta/320x480_2.png?raw=true "320x480")


## 24. top_bottom_zoom
-------------------------
### 300x250
![Alt text](templates/top_bottom_zoom/300x250_1.png?raw=true "300x250")
![Alt text](templates/top_bottom_zoom/300x250_2.png?raw=true "300x250")


## 25. parallax_slider
-------------------------
### 320x480
![Alt text](templates/parallax_slider/320x480.png?raw=true "320x480")


## 26. focused_flipper
-------------------------
### 320x480
![Alt text](templates/focused_flipper/320x480_1.png?raw=true "320x480")
![Alt text](templates/focused_flipper/320x480_2.png?raw=true "320x480")
